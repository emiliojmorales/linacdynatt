import sys
import os
from shutil import copyfile

import time
import traceback
from datetime import datetime

import tango as tg
import fandango as fn
from taurus.external.qt import QtWidgets

import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  LinacDynAtt
#  Copyright (C) 2021  Controls
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


class SendEmail(object):

    def __init__(self, option, plc, parent=None):

        super(SendEmail, self).__init__()

        subject = "Linac PLC DynamicAttributes modified"

        self.option = option
        self.plc = plc
        msg = self.create_msg(self.plc)

        try:
            self.send_mail(msg, subject)

        except Exception as e:
            print("We can not send a e-mail. Please check.")
            traceback.print_exc()

    def send_mail(self, msg_body, subject=None, send_to=None, files=None):
        '''
        Method to send e-mail messages once script finish.
        In case of error you can attach a log file to send.
        
        :param msg_body: Test to be insterted as body message.
        :param send_to: list of receivers
        :param subject:
        :param files: Log file to send
        :return: Nothing.
        '''
        # Define the smptep server
        server = 'smtp.cells.es'
        # Define the sender e-mail
        send_from = 'root@crremote01.cells.es'
        # Define the receivers. Always a list.
        if send_to is None:
            send_to = ["emorales@cells.es", "dlanaia@cells.es"]
            # send_to = ["emorales@cells.es"]
        else:
            send_to = send_to

        # Start to write message:
        msg = MIMEMultipart()
        msg['From'] = 'Control Room <' + send_from + '>'
        msg['Bcc'] = COMMASPACE.join(send_to)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject

        if files is None:
            msg.attach(MIMEText(msg_body))

        if files is not None:
            msg.attach(MIMEText(msg_body))

            # Attach file start here:
            with open(files, "rb") as f:
                part = MIMEApplication(
                    f.read(),
                    Name=os.path.basename(files)
                )
            # After the file is closed
            part['Content-Disposition'] = 'attachment; filename="%s"'\
                                          % os.path.basename(files)
            # Attach file
            msg.attach(part)

        # Send email:
        smtp = smtplib.SMTP(server)
        smtp.sendmail(send_from, send_to, msg.as_string())
        smtp.close()

    def create_msg(self, plc):

        if self.option == "R-all":

            msg = "*** This mail is generated automatically *** \n\n" \
                  "All Linac PLC DS DynamicAttributes property recovered from" \
                  " backup files. " \
                  "\n\n" \
                  "Have a nice Day. \n" \
                  "CT4 Group. \n" \
                  "Controls section."

        elif self.option == "U":

            msg = "*** This mail is generated automatically *** \n\n" \
                  "Linac DS {} DynamicAttributes property changed from " \
                  "user defined file. \n" \
                  "In case of problems remember that a " \
                  "copy of previous state is saved in a Backup folder." \
                  "\n\n" \
                  "Have a nice Day. \n" \
                  "CT4 Group. \n" \
                  "Controls section.".format(plc)

        elif self.option == "P":

            msg = "*** This mail is generated automatically *** \n\n" \
                  "Errors loading Linac DS DynamicAttributes property \n" \
                  "Please check. " \
                  "\n\n" \
                  "Have a nice Day. \n" \
                  "CT4 Group. \n" \
                  "Controls section."

        elif self.option == "R-one":

            msg = "*** This mail is generated automatically *** \n\n" \
                  "Linac {} DS DynamicAttributes property recovered from " \
                  "backup files. " \
                  "\n\n" \
                  "Have a nice Day. \n" \
                  "CT4 Group. \n" \
                  "Controls section.".format(plc)

        return msg


class RecoverDynAtt(object):
    
    _files = None
    _property = "DynamicAttributes"
    _path = "/homelocal/operator/src/Linac/Backup/"
    _files_path = None
    _file_path = None
    _dyn_att = {}
    _only_one = None
    _done = False

    def __init__(self, only_one):
        
        super(RecoverDynAtt, self).__init__()

        self._only_one = only_one
        self._problems_dict = {}

        if not self._only_one:
            self.recover_all()

    def obtain_file(self):

        try:
            dlg = QtWidgets.QFileDialog()
            aux = dlg.getOpenFileName(caption='Choose File to recover',
                                      directory=self._path)
            if aux:
                self._files = str(aux)
            else:
                self._done = False
                raise ValueError("No File selected!! Please select a file.")

        except Exception as e:
            self._done = False
            print("Problems in obtain_file: {}".format(e))
            traceback.print_exc()

    def obtain_files(self):

        try:
            dlg = QtWidgets.QFileDialog()
            aux = dlg.getExistingDirectory(caption='Choose Folder to recover',
                                           directory=self._path)
            if aux:
                self._files_path = str(aux)
                self._files = os.listdir(str(aux))
            else:
                self._done = False
                raise ValueError("No Folder selected!! Please select a folder.")

        except Exception as e:
            self._done = False
            print("Problems in obtain_files: {}".format(e))
            traceback.print_exc()

    def obtain_data(self):

        try:
            if isinstance(self._files, list):
                
                for f_name in self._files:
                    
                    point = f_name.find('.')
                    aux = str(f_name[:point])
                    device = aux.replace('_', '/')
                    
                    f_to_open = "{}/{}".format(self._files_path, f_name)
                    
                    content = []
                    with open(str(f_to_open), 'r') as f:
                        for line in f:
                            content.append(line.rstrip('\n'))
                    f.close()
                    self._dyn_att[device] = [content[1:]]
            
            elif isinstance(self._files, str):
                
                aux = os.path.basename(self._files)
                point = aux.find('.')
                aux = str(aux[:point])
                device = aux.replace('_', '/')
                
                content = []
                with open(str(self._files), 'r') as f:
                    for line in f:
                        content.append(line.rstrip('\n'))
                f.close()
                self._dyn_att[device] = [content[1:]]
                
                return device

        except Exception as e:
            self._done = False
            print("Problems in obtain_data: {}".format(e))
            traceback.print_exc()

    def recover_dyn_att(self):

        try:
            for device in self._dyn_att.keys():
                
                time.sleep(0.1)
                dp_plc = tg.DeviceProxy(device)
                dyn_att = self._dyn_att[device][0]
                to_write = {self._property: dyn_att}
                dp_plc.put_property(to_write)
                time.sleep(0.2)
                problems = not dp_plc.updateDynamicAttributes()
                time.sleep(0.3)

                if problems:
                    error = dp_plc.read_attribute("DynamicAttributes_errors")
                    self._problems_dict[device] = error.value
            
            if self._problems_dict:
                self._done = False
                raise ValueError("Problems in one or more PLC Dynamic "
                                 "attributes.")
            else:
                if self._dyn_att:
                    self._done = True
                else:
                    self._done = False
                    

        except ValueError:

            traceback.print_exc()
            print("\n\n Problems detected: \n")

            for plc in self._problems_dict.keys():
                problem_text = self._problems_dict[plc]
                print("\t {}: {}".format(plc, problem_text))

            print("\n")

        except Exception as e:
            print("Problems at recover_dyn_att: {}".format(e))
            self._done = False
            traceback.print_exc()

    def recover_all(self):

        try:
            self.obtain_files()
            self.obtain_data()
            self.recover_dyn_att()
            
            if self._done:
                print("All PLC DynamicAttributes recovered!")
                SendEmail("R-all", None)
            else:
                SendEmail("P", None)
                print("We had problems recovering all PLC DynamicAttributes. "
                      "Please check.")

        except Exception as e:
            print("We can not recover DynamicAttributes for ALL PLC.")
            traceback.print_exc()


class WriteUserDynAtt(object):

    _plc_list = []
    _file_names = []
    _dyn_att = {}
    _date = None
    _property = "DynamicAttributes"
    _path = ""

    def __init__(self, parent=None):

        super(WriteUserDynAtt, self).__init__()

        self._problems_dict = {}
        self._done = False
        # Obtain PLC list:
        self._plc_list = [plc for plc in fn.get_matching_devices("li/ct/plc*")
                          if "sim" not in plc]

        try:
            self.write_dyn_att_from_file()

            if self._done:
                plc = list(self._dyn_att.keys())
                print("DynamicAttributes property from {} updated."
                      .format(plc[0]))
                SendEmail("U", plc[0])
            else:
                plc = list(self._dyn_att.keys())
                SendEmail("P", None)

        except Exception as e:
            print("Write user dynamic attributes problem. Please Check.")
            traceback.print_exc()

    def obtain_files(self):

        try:
            dlg = QtWidgets.QFileDialog()
            aux = dlg.getOpenFileNames(caption='Choose PLC config files',
                                       directory=self._path)
            if len(aux) > 0:
                self._file_names = [str(aux[0])]
            else:
                raise ValueError("No file selected!! Please select a file.")

        except Exception as e:
            print("Problems in obtain files: {}".format(e))
            traceback.print_exc()

    def obtain_plc_device(self):

        try:
            for f_name in self._file_names:
                with open(f_name, 'r') as f:
                    # First line will contain PLC device name.
                    aux = f.readline()
                f.close()
                plc_device = aux[1:-1]
                self._dyn_att[plc_device] = [f_name]

        except Exception as e:
            print("Problems in obtain_plc_device: {}".format(e))
            traceback.print_exc()

    def write_dyn_att_from_file(self):
        try:
            self.obtain_files()

            if len(self._file_names) > 0:
                self.obtain_plc_device()

                if self._dyn_att:
                    for device in self._dyn_att.keys():
                        try:
                            dp_plc = tg.DeviceProxy(device)
                            dyn_att = self.read_file(self._dyn_att[device]
                                                     [0])
                            to_write = {self._property: dyn_att}
                            dp_plc.put_property(to_write)
                            time.sleep(0.2)
                            problems = not dp_plc.updateDynamicAttributes()
                            time.sleep(0.3)

                            if problems:
                                error = dp_plc.read_attribute(
                                    "DynamicAttributes_errors")
                                self._problems_dict[device] = error.value

                        except Exception as e:
                            print("Problems writing property: {}".format(e))
                            self._done = False
                            traceback.print_exc()

                if self._problems_dict:
                    self._done = False
                    raise ValueError("Problems in one or more PLC "
                                     "Dynamic attributes.")
                else:
                    self._done = True

            else:
                raise TypeError("No file selected! We can not obtain PLC "
                                 "devices")

        except ValueError:

            traceback.print_exc()
            print("\n\n Problems detected: \n")

            for plc in self._problems_dict.keys():
                problem_text = self._problems_dict[plc]
                print("\t {}: {}".format(plc, problem_text))

            print("\n")

        except Exception as e:

            print("Problems at write_dyn_att_from_file: {}".format(e))
            traceback.print_exc()

    def read_file(self, l_file):

        try:
            content = []
            with open(l_file, 'r') as f:
                for line in f:
                    content.append(line.rstrip('\n'))
            f.close()
            return content[1:]

        except Exception as e:
            print("Problems reading {} file: {}".format(l_file, e))
            traceback.print_exc()


class SaveDynAtt(object):

    _plc_list = []
    _file_names = []
    _dyn_att = {}
    _day = None
    _hour = None
    _property = "DynamicAttributes"
    _path = "/data/LINAC/DynamicAttributes/PLC_Backup/"
    _git_path = "/data/LINAC/DynamicAttributes/.controls/"
    _file_path = ""

    def __init__(self, parent=None):

        super(SaveDynAtt, self).__init__()

        self._problems_dict = {}
        self._plc_list = []
        self._file_names = []
        
        # Obtain PLC list:
        self._plc_list = [plc.lower() for plc in
                          fn.get_matching_devices("li/ct/plc*")
                          if "sim" not in plc]

        # Date: To be used day folder creation:
        self._day = datetime.now().strftime("%Y%m%d")
        self._hour = datetime.now().strftime("%Hh%M")

        self.create_day_folder(day=True, hour=False)

        # File names creation from
        for name in self._plc_list:
            aux = name.replace('/', '_')
            self._file_names.append("{}.linac".format(aux))

        # Obtain Dynamic Attributes from every PLC:
        self.obtain_dyn_att()

        # Save  on a file:
        self.save_files()

        # Copy to Git folder:
        self.copy_to_git()

    def obtain_dyn_att(self):
        try:
            for device in self._plc_list:

                print("Reading DynamicAttributes from {}".format(device))
                ds_plc = tg.DeviceProxy(device)
                if ds_plc.updateDynamicAttributes():
                    dynAtt = ds_plc.get_property(self._property)
                    self._dyn_att[device] = dynAtt[self._property]
                    print("Done!")
                    time.sleep(0.5)
                else:
                    print("{} not saved!".format(device))
                    error = ds_plc.read_attribute("DynamicAttributes_errors")
                    self._problems_dict[device] = error.value

            if self._problems_dict:
                raise ValueError("Problems in one or more PLC "
                                 "DynamicAttributes.")
        except ValueError:

            traceback.print_exc()
            print("\n\n Problems detected: \n")

            for plc in self._problems_dict.keys():
                problem_text = self._problems_dict[plc]
                print("\t {}: {}".format(plc, problem_text))

            print("\n")

        except Exception as e:
            print("Problems in obtain_dyn_att: {}".format(e))
            traceback.print_exc()

    def save_files(self):

        self.create_day_folder(day=False, hour=True)
        pos = None
        f_name = None

        for pos, fname in enumerate(self._file_names):
            path = "{}/{}".format(self._file_path, fname)
            try:
                key = self._plc_list[pos]
                if key in self._dyn_att.keys():
                    with open(path, 'w') as f:
                        dynAtt = self._dyn_att[key]
                        first_line = "#{}".format(key)
                        f.write(first_line)
                        f.write('\n')
                        for line in dynAtt:
                            f.write(line)
                            f.write('\n')
                    f.close()
                time.sleep(0.2)

            except Exception as e:
                print("Problems in save_files: {}".format(e))
                traceback.print_exc()

    def create_day_folder(self, day=True, hour=False):

        if day:
            path = "{}{}".format(self._path, self._day)
            try:
                if not os.path.exists(path):
                    print("Creating day folder: ".format(path))
                    os.mkdir(path)
                    print("Done!")
                else:
                    print("Day folder already created!")

            except Exception as e:
                print("Problems creating day folder: ".format(e))
                traceback.print_exc()

        elif hour:
            self._file_path = "{}{}/{}".format(self._path, self._day,
                                               self._hour)
            try:
                if not os.path.exists(self._file_path):
                    print("Creating hour folder: ".format(self._file_path))
                    os.mkdir(self._file_path)
                    print("Done!")
                else:
                    print("Hour folder already created!")

            except Exception as e:
                print("Problems creating hour folder: ".format(e))
                traceback.print_exc()

    def copy_to_git(self):

        # Go to last fiel path:
        print("Change to path: {}".format(self._file_path))
        os.chdir(self._file_path)
        # Copy files to .controls folder:
        for fname in self._file_names:
            try:
                filename = os.path.basename(fname)
                copyfile(fname, os.path.join(self._git_path, filename))
            except Exception as e:
                print("Problems trying to copy files to git controls"
                      "folder: {} \n {}".format(e, traceback.format_exc()))

        # Commit files:
        gitForControls()


class gitForControls(object):

    _git_path = "/data/LINAC/DynamicAttributes/.controls/"

    def __init__(self):

        super(gitForControls, self).__init__()

        self.commit_files()

    def commit_files(self):

        # Go to Git repo location:
        os.chdir(self._git_path)

        # Commit message will be hour:
        hour = datetime.now().strftime("%Y%m%d %Hh%M")
        # Git message:
        message = "{} - New files version".format(hour)

        try:
            os.system("git commit -a -m '{}'".format(message))

        except Exception as e:
            print("{} \n {}".format(e, traceback.format_exc()))



