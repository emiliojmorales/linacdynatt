import sys
import os

from taurus.external.qt import QtWidgets, uic
import traceback

from LinacDynAtt import SaveDynAtt
from LinacDynAtt import WriteUserDynAtt
from LinacDynAtt import RecoverDynAtt

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  LinacDynAtt
#  Copyright (C) 2021  E.Morales
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

UI_FILE_PATH = os.path.join(os.path.dirname(__file__), "ui", "dynatt.ui")
UI_OBJECT = uic.loadUiType(UI_FILE_PATH)[0]


class DynAttLoader(QtWidgets.QWidget, UI_OBJECT):

    _active_chb = None
    _ch_box = None

    def __init__(self, parent=None):

        super(DynAttLoader, self).__init__(parent)

        self.setupUi(self)
        self.setWindowTitle("Linac DynAtt GUI.")

        # UI Check box list:
        self._ch_box = [self.chbx_save, self.chbx_read, self.chbx_recover_all]

        # Define connections
        self.pb_ok.clicked.connect(self.start)
        self.pb_cancel.clicked.connect(self.cancel)

    def start(self):

        # Get active check box:
        self._active_chb = self.get_check_box()

        if self._active_chb == "save":
            self.save_parameters()
        elif self._active_chb == "load":
            self.load_parameters()
        elif self._active_chb == "recover-all":
            self.recover_all()
        else:
            raise TypeError("\n***Error: Parameter missing.***"
                            " \n\n"
                            "To run this program we need 1 parameter. "
                            "Please, select 1 check box and push start. \n ")

    def get_check_box(self):

        try:
            for pos, ch_b in enumerate(self._ch_box):
                if ch_b.isChecked():
                    if pos == 0:
                        return "save"
                    elif pos == 1:
                        return "load"
                    elif pos == 2:
                        return "recover-all"
                    else:
                        raise TypeError("Check box not filtered")

        except Exception as e:
            print("Problems in get_check_box: {}".format(e))
            traceback.print_exc()

    def cancel(self):
        sys.exit()

    def save_parameters(self):
        try:
            SaveDynAtt()

        except Exception as e:
            print("We can not save parameters. Check: {}".format(e))
            traceback.print_exc()

    def load_parameters(self):
        try:
            SaveDynAtt()
            WriteUserDynAtt()

        except Exception as e:
            print("We can not load parameters. Check: {}".format(e))
            traceback.print_exc()

    def recover_all(self):
        try:
            RecoverDynAtt(only_one=False)

        except Exception as e:
            print("We can not recover PLC All PLC. Check: {}".format(e))
            traceback.print_exc()

def main():
    app = QtWidgets.QApplication(sys.argv)
    MyWindow = DynAttLoader(None)
    MyWindow.show()
    app.exec_()


if __name__ == "__main__":

    main()
